package ie.letsgetcheckedtask;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import ie.letsgetcheckedtask.databinding.CardCountriesBindingImpl;
import ie.letsgetcheckedtask.databinding.DialogCountryDetailsBindingImpl;
import ie.letsgetcheckedtask.databinding.DialogPositiveOnlyBindingImpl;
import ie.letsgetcheckedtask.databinding.FragViewCountriesListBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_CARDCOUNTRIES = 1;

  private static final int LAYOUT_DIALOGCOUNTRYDETAILS = 2;

  private static final int LAYOUT_DIALOGPOSITIVEONLY = 3;

  private static final int LAYOUT_FRAGVIEWCOUNTRIESLIST = 4;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(4);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(ie.letsgetcheckedtask.R.layout.card_countries, LAYOUT_CARDCOUNTRIES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(ie.letsgetcheckedtask.R.layout.dialog_country_details, LAYOUT_DIALOGCOUNTRYDETAILS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(ie.letsgetcheckedtask.R.layout.dialog_positive_only, LAYOUT_DIALOGPOSITIVEONLY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(ie.letsgetcheckedtask.R.layout.frag_view_countries_list, LAYOUT_FRAGVIEWCOUNTRIESLIST);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_CARDCOUNTRIES: {
          if ("layout/card_countries_0".equals(tag)) {
            return new CardCountriesBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for card_countries is invalid. Received: " + tag);
        }
        case  LAYOUT_DIALOGCOUNTRYDETAILS: {
          if ("layout/dialog_country_details_0".equals(tag)) {
            return new DialogCountryDetailsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for dialog_country_details is invalid. Received: " + tag);
        }
        case  LAYOUT_DIALOGPOSITIVEONLY: {
          if ("layout/dialog_positive_only_0".equals(tag)) {
            return new DialogPositiveOnlyBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for dialog_positive_only is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGVIEWCOUNTRIESLIST: {
          if ("layout/frag_view_countries_list_0".equals(tag)) {
            return new FragViewCountriesListBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for frag_view_countries_list is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(5);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "binderModel");
      sKeys.put(2, "cardModel");
      sKeys.put(3, "dialogBinder");
      sKeys.put(4, "dialogModel");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(4);

    static {
      sKeys.put("layout/card_countries_0", ie.letsgetcheckedtask.R.layout.card_countries);
      sKeys.put("layout/dialog_country_details_0", ie.letsgetcheckedtask.R.layout.dialog_country_details);
      sKeys.put("layout/dialog_positive_only_0", ie.letsgetcheckedtask.R.layout.dialog_positive_only);
      sKeys.put("layout/frag_view_countries_list_0", ie.letsgetcheckedtask.R.layout.frag_view_countries_list);
    }
  }
}

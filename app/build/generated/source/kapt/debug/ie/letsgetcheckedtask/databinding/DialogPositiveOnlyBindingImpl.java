package ie.letsgetcheckedtask.databinding;
import ie.letsgetcheckedtask.R;
import ie.letsgetcheckedtask.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class DialogPositiveOnlyBindingImpl extends DialogPositiveOnlyBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.TextView mboundView1;
    @NonNull
    private final android.widget.TextView mboundView2;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public DialogPositiveOnlyBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds));
    }
    private DialogPositiveOnlyBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.Button) bindings[3]
            );
        this.button.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.TextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.dialogModel == variableId) {
            setDialogModel((ie.letsgetcheckedtask.models.binderModels.DialogPositiveModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setDialogModel(@Nullable ie.letsgetcheckedtask.models.binderModels.DialogPositiveModel DialogModel) {
        this.mDialogModel = DialogModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.dialogModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean dialogModelButtonTextJavaLangObjectNull = false;
        java.lang.String dialogModelButtonTextJavaLangObjectNullDialogModelButtonTextButtonAndroidStringTxtBtnDismiss = null;
        android.view.View.OnClickListener dialogModelClickListener = null;
        ie.letsgetcheckedtask.models.binderModels.DialogPositiveModel dialogModel = mDialogModel;
        java.lang.String dialogModelTitle = null;
        java.lang.String dialogModelButtonText = null;
        java.lang.String dialogModelMessage = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (dialogModel != null) {
                    // read dialogModel.clickListener
                    dialogModelClickListener = dialogModel.getClickListener();
                    // read dialogModel.title
                    dialogModelTitle = dialogModel.getTitle();
                    // read dialogModel.buttonText
                    dialogModelButtonText = dialogModel.getButtonText();
                    // read dialogModel.message
                    dialogModelMessage = dialogModel.getMessage();
                }


                // read dialogModel.buttonText != null
                dialogModelButtonTextJavaLangObjectNull = (dialogModelButtonText) != (null);
            if((dirtyFlags & 0x3L) != 0) {
                if(dialogModelButtonTextJavaLangObjectNull) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x3L) != 0) {

                // read dialogModel.buttonText != null ? dialogModel.buttonText : @android:string/txt_btn_dismiss
                dialogModelButtonTextJavaLangObjectNullDialogModelButtonTextButtonAndroidStringTxtBtnDismiss = ((dialogModelButtonTextJavaLangObjectNull) ? (dialogModelButtonText) : (button.getResources().getString(R.string.txt_btn_dismiss)));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.button.setOnClickListener(dialogModelClickListener);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.button, dialogModelButtonTextJavaLangObjectNullDialogModelButtonTextButtonAndroidStringTxtBtnDismiss);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, dialogModelTitle);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, dialogModelMessage);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): dialogModel
        flag 1 (0x2L): null
        flag 2 (0x3L): dialogModel.buttonText != null ? dialogModel.buttonText : @android:string/txt_btn_dismiss
        flag 3 (0x4L): dialogModel.buttonText != null ? dialogModel.buttonText : @android:string/txt_btn_dismiss
    flag mapping end*/
    //end
}
package ie.letsgetcheckedtask;

public class BR {
  public static final int _all = 0;

  public static final int binderModel = 1;

  public static final int cardModel = 2;

  public static final int dialogBinder = 3;

  public static final int dialogModel = 4;
}

package ie.letsgetcheckedtask.databinding;
import ie.letsgetcheckedtask.R;
import ie.letsgetcheckedtask.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class DialogCountryDetailsBindingImpl extends DialogCountryDetailsBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.linearLayout, 8);
        sViewsWithIds.put(R.id.linearLayout2, 9);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.TextView mboundView2;
    @NonNull
    private final android.widget.TextView mboundView3;
    @NonNull
    private final android.widget.TextView mboundView4;
    @NonNull
    private final android.widget.TextView mboundView5;
    // variables
    // values
    // listeners
    private OnClickListenerImpl mDialogBinderFavoriteClickListenerOnFavoriteClickedAndroidViewViewOnClickListener;
    // Inverse Binding Event Handlers

    public DialogCountryDetailsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 10, sIncludes, sViewsWithIds));
    }
    private DialogCountryDetailsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.Button) bindings[6]
            , (com.google.android.material.button.MaterialButton) bindings[7]
            , (android.widget.LinearLayout) bindings[8]
            , (android.widget.LinearLayout) bindings[9]
            , (android.widget.TextView) bindings[1]
            );
        this.btnDismiss.setTag(null);
        this.btnFavorite.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (android.widget.TextView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (android.widget.TextView) bindings[5];
        this.mboundView5.setTag(null);
        this.textView5.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.dialogBinder == variableId) {
            setDialogBinder((ie.letsgetcheckedtask.models.binderModels.DialogCountryBinderModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setDialogBinder(@Nullable ie.letsgetcheckedtask.models.binderModels.DialogCountryBinderModel DialogBinder) {
        this.mDialogBinder = DialogBinder;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.dialogBinder);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String dialogBinderRegion = null;
        boolean dialogBinderFavorite = false;
        java.lang.String dialogBinderRegionEmptyMboundView3AndroidStringTxtNotAvailableDialogBinderRegion = null;
        boolean dialogBinderRegionEmpty = false;
        ie.letsgetcheckedtask.models.binderModels.CardBinderModel dialogBinderCardBinderModel = null;
        java.lang.String dialogBinderSubRegion = null;
        java.lang.String dialogBinderFavoriteBtnFavoriteAndroidStringTxtBtnRemoveBtnFavoriteAndroidStringTxtBtnAdd = null;
        java.lang.String dialogBinderName = null;
        boolean dialogBinderSubRegionEmpty = false;
        java.lang.String dialogBinderCapitalEmptyMboundView2AndroidStringTxtNotAvailableDialogBinderCapital = null;
        android.view.View.OnClickListener dialogBinderFavoriteClickListenerOnFavoriteClickedAndroidViewViewOnClickListener = null;
        java.lang.String dialogBinderCapital = null;
        java.lang.String dialogBinderSubRegionEmptyMboundView4AndroidStringTxtNotAvailableDialogBinderSubRegion = null;
        java.lang.String dialogBinderPopulationEmptyMboundView5AndroidStringTxtNotAvailableDialogBinderPopulation = null;
        ie.letsgetcheckedtask.models.binderModels.DialogCountryBinderModel dialogBinder = mDialogBinder;
        ie.letsgetcheckedtask.supportClasses.interfaces.IFavoriteAction dialogBinderFavoriteClickListener = null;
        int dialogBinderFavoriteBtnFavoriteAndroidColorColorErrorBtnFavoriteAndroidColorColorPrimaryDark = 0;
        boolean dialogBinderPopulationEmpty = false;
        boolean dialogBinderCapitalEmpty = false;
        android.view.View.OnClickListener dialogBinderDismissListener = null;
        java.lang.String dialogBinderPopulation = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (dialogBinder != null) {
                    // read dialogBinder.region
                    dialogBinderRegion = dialogBinder.getRegion();
                    // read dialogBinder.favorite
                    dialogBinderFavorite = dialogBinder.getFavorite();
                    // read dialogBinder.cardBinderModel
                    dialogBinderCardBinderModel = dialogBinder.getCardBinderModel();
                    // read dialogBinder.subRegion
                    dialogBinderSubRegion = dialogBinder.getSubRegion();
                    // read dialogBinder.name
                    dialogBinderName = dialogBinder.getName();
                    // read dialogBinder.capital
                    dialogBinderCapital = dialogBinder.getCapital();
                    // read dialogBinder.favoriteClickListener
                    dialogBinderFavoriteClickListener = dialogBinder.getFavoriteClickListener();
                    // read dialogBinder.dismissListener
                    dialogBinderDismissListener = dialogBinder.getDismissListener();
                    // read dialogBinder.population
                    dialogBinderPopulation = dialogBinder.getPopulation();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(dialogBinderFavorite) {
                        dirtyFlags |= 0x20L;
                        dirtyFlags |= 0x2000L;
                }
                else {
                        dirtyFlags |= 0x10L;
                        dirtyFlags |= 0x1000L;
                }
            }


                if (dialogBinderRegion != null) {
                    // read dialogBinder.region.empty
                    dialogBinderRegionEmpty = dialogBinderRegion.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(dialogBinderRegionEmpty) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }
                // read dialogBinder.favorite ? @android:string/txt_btn_remove : @android:string/txt_btn_add
                dialogBinderFavoriteBtnFavoriteAndroidStringTxtBtnRemoveBtnFavoriteAndroidStringTxtBtnAdd = ((dialogBinderFavorite) ? (btnFavorite.getResources().getString(R.string.txt_btn_remove)) : (btnFavorite.getResources().getString(R.string.txt_btn_add)));
                // read dialogBinder.favorite ? @android:color/colorError : @android:color/colorPrimaryDark
                dialogBinderFavoriteBtnFavoriteAndroidColorColorErrorBtnFavoriteAndroidColorColorPrimaryDark = ((dialogBinderFavorite) ? (getColorFromResource(btnFavorite, R.color.colorError)) : (getColorFromResource(btnFavorite, R.color.colorPrimaryDark)));
                if (dialogBinderSubRegion != null) {
                    // read dialogBinder.subRegion.empty
                    dialogBinderSubRegionEmpty = dialogBinderSubRegion.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(dialogBinderSubRegionEmpty) {
                        dirtyFlags |= 0x200L;
                }
                else {
                        dirtyFlags |= 0x100L;
                }
            }
                if (dialogBinderCapital != null) {
                    // read dialogBinder.capital.empty
                    dialogBinderCapitalEmpty = dialogBinderCapital.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(dialogBinderCapitalEmpty) {
                        dirtyFlags |= 0x80L;
                }
                else {
                        dirtyFlags |= 0x40L;
                }
            }
                if (dialogBinderFavoriteClickListener != null) {
                    // read dialogBinder.favoriteClickListener::onFavoriteClicked
                    dialogBinderFavoriteClickListenerOnFavoriteClickedAndroidViewViewOnClickListener = (((mDialogBinderFavoriteClickListenerOnFavoriteClickedAndroidViewViewOnClickListener == null) ? (mDialogBinderFavoriteClickListenerOnFavoriteClickedAndroidViewViewOnClickListener = new OnClickListenerImpl()) : mDialogBinderFavoriteClickListenerOnFavoriteClickedAndroidViewViewOnClickListener).setValue(dialogBinderFavoriteClickListener));
                }
                if (dialogBinderPopulation != null) {
                    // read dialogBinder.population.empty
                    dialogBinderPopulationEmpty = dialogBinderPopulation.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(dialogBinderPopulationEmpty) {
                        dirtyFlags |= 0x800L;
                }
                else {
                        dirtyFlags |= 0x400L;
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x3L) != 0) {

                // read dialogBinder.region.empty ? @android:string/txt_not_available : dialogBinder.region
                dialogBinderRegionEmptyMboundView3AndroidStringTxtNotAvailableDialogBinderRegion = ((dialogBinderRegionEmpty) ? (mboundView3.getResources().getString(R.string.txt_not_available)) : (dialogBinderRegion));
                // read dialogBinder.capital.empty ? @android:string/txt_not_available : dialogBinder.capital
                dialogBinderCapitalEmptyMboundView2AndroidStringTxtNotAvailableDialogBinderCapital = ((dialogBinderCapitalEmpty) ? (mboundView2.getResources().getString(R.string.txt_not_available)) : (dialogBinderCapital));
                // read dialogBinder.subRegion.empty ? @android:string/txt_not_available : dialogBinder.subRegion
                dialogBinderSubRegionEmptyMboundView4AndroidStringTxtNotAvailableDialogBinderSubRegion = ((dialogBinderSubRegionEmpty) ? (mboundView4.getResources().getString(R.string.txt_not_available)) : (dialogBinderSubRegion));
                // read dialogBinder.population.empty ? @android:string/txt_not_available : dialogBinder.population
                dialogBinderPopulationEmptyMboundView5AndroidStringTxtNotAvailableDialogBinderPopulation = ((dialogBinderPopulationEmpty) ? (mboundView5.getResources().getString(R.string.txt_not_available)) : (dialogBinderPopulation));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.btnDismiss.setOnClickListener(dialogBinderDismissListener);
            this.btnFavorite.setOnClickListener(dialogBinderFavoriteClickListenerOnFavoriteClickedAndroidViewViewOnClickListener);
            this.btnFavorite.setTag(dialogBinderCardBinderModel);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btnFavorite, dialogBinderFavoriteBtnFavoriteAndroidStringTxtBtnRemoveBtnFavoriteAndroidStringTxtBtnAdd);
            this.btnFavorite.setStrokeColor(androidx.databinding.adapters.Converters.convertColorToColorStateList(dialogBinderFavoriteBtnFavoriteAndroidColorColorErrorBtnFavoriteAndroidColorColorPrimaryDark));
            this.btnFavorite.setTextColor(dialogBinderFavoriteBtnFavoriteAndroidColorColorErrorBtnFavoriteAndroidColorColorPrimaryDark);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, dialogBinderCapitalEmptyMboundView2AndroidStringTxtNotAvailableDialogBinderCapital);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, dialogBinderRegionEmptyMboundView3AndroidStringTxtNotAvailableDialogBinderRegion);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, dialogBinderSubRegionEmptyMboundView4AndroidStringTxtNotAvailableDialogBinderSubRegion);
            ie.letsgetcheckedtask.supportClasses.BinderAdaptersKt.formatPopulation(this.mboundView5, dialogBinderPopulationEmptyMboundView5AndroidStringTxtNotAvailableDialogBinderPopulation);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView5, dialogBinderName);
        }
    }
    // Listener Stub Implementations
    public static class OnClickListenerImpl implements android.view.View.OnClickListener{
        private ie.letsgetcheckedtask.supportClasses.interfaces.IFavoriteAction value;
        public OnClickListenerImpl setValue(ie.letsgetcheckedtask.supportClasses.interfaces.IFavoriteAction value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onClick(android.view.View arg0) {
            this.value.onFavoriteClicked(arg0); 
        }
    }
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): dialogBinder
        flag 1 (0x2L): null
        flag 2 (0x3L): dialogBinder.region.empty ? @android:string/txt_not_available : dialogBinder.region
        flag 3 (0x4L): dialogBinder.region.empty ? @android:string/txt_not_available : dialogBinder.region
        flag 4 (0x5L): dialogBinder.favorite ? @android:string/txt_btn_remove : @android:string/txt_btn_add
        flag 5 (0x6L): dialogBinder.favorite ? @android:string/txt_btn_remove : @android:string/txt_btn_add
        flag 6 (0x7L): dialogBinder.capital.empty ? @android:string/txt_not_available : dialogBinder.capital
        flag 7 (0x8L): dialogBinder.capital.empty ? @android:string/txt_not_available : dialogBinder.capital
        flag 8 (0x9L): dialogBinder.subRegion.empty ? @android:string/txt_not_available : dialogBinder.subRegion
        flag 9 (0xaL): dialogBinder.subRegion.empty ? @android:string/txt_not_available : dialogBinder.subRegion
        flag 10 (0xbL): dialogBinder.population.empty ? @android:string/txt_not_available : dialogBinder.population
        flag 11 (0xcL): dialogBinder.population.empty ? @android:string/txt_not_available : dialogBinder.population
        flag 12 (0xdL): dialogBinder.favorite ? @android:color/colorError : @android:color/colorPrimaryDark
        flag 13 (0xeL): dialogBinder.favorite ? @android:color/colorError : @android:color/colorPrimaryDark
    flag mapping end*/
    //end
}
package ie.letsgetcheckedtask.databinding;
import ie.letsgetcheckedtask.R;
import ie.letsgetcheckedtask.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class CardCountriesBindingImpl extends CardCountriesBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.linearLayout4, 5);
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    @NonNull
    private final android.widget.TextView mboundView2;
    @NonNull
    private final android.widget.TextView mboundView3;
    // variables
    // values
    // listeners
    private OnClickListenerImpl mCardModelFavoriteClickListenerOnFavoriteClickedAndroidViewViewOnClickListener;
    private OnClickListenerImpl1 mCardModelCardClickListenerOnCardClickedAndroidViewViewOnClickListener;
    // Inverse Binding Event Handlers

    public CardCountriesBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private CardCountriesBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageView) bindings[4]
            , (android.widget.LinearLayout) bindings[5]
            , (android.widget.TextView) bindings[1]
            );
        this.imFavoriteIcon.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        this.textView6.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.cardModel == variableId) {
            setCardModel((ie.letsgetcheckedtask.models.binderModels.CardBinderModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setCardModel(@Nullable ie.letsgetcheckedtask.models.binderModels.CardBinderModel CardModel) {
        this.mCardModel = CardModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.cardModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        android.view.View.OnClickListener cardModelFavoriteClickListenerOnFavoriteClickedAndroidViewViewOnClickListener = null;
        ie.letsgetcheckedtask.models.binderModels.CardBinderModel cardModel = mCardModel;
        java.lang.String cardModelCountryModelName = null;
        ie.letsgetcheckedtask.supportClasses.interfaces.IFavoriteAction cardModelFavoriteClickListener = null;
        boolean cardModelCountryModelFavorite = false;
        ie.letsgetcheckedtask.supportClasses.interfaces.ICardClickListener cardModelCardClickListener = null;
        java.lang.String cardModelCountryModelRegion = null;
        ie.letsgetcheckedtask.models.CountryModel cardModelCountryModel = null;
        android.graphics.drawable.Drawable cardModelCountryModelFavoriteImFavoriteIconAndroidDrawableIcFavoriteFilledImFavoriteIconAndroidDrawableIcFavoriteOutline = null;
        java.lang.String cardModelCountryModelCapital = null;
        android.view.View.OnClickListener cardModelCardClickListenerOnCardClickedAndroidViewViewOnClickListener = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (cardModel != null) {
                    // read cardModel.favoriteClickListener
                    cardModelFavoriteClickListener = cardModel.getFavoriteClickListener();
                    // read cardModel.cardClickListener
                    cardModelCardClickListener = cardModel.getCardClickListener();
                    // read cardModel.countryModel
                    cardModelCountryModel = cardModel.getCountryModel();
                }


                if (cardModelFavoriteClickListener != null) {
                    // read cardModel.favoriteClickListener::onFavoriteClicked
                    cardModelFavoriteClickListenerOnFavoriteClickedAndroidViewViewOnClickListener = (((mCardModelFavoriteClickListenerOnFavoriteClickedAndroidViewViewOnClickListener == null) ? (mCardModelFavoriteClickListenerOnFavoriteClickedAndroidViewViewOnClickListener = new OnClickListenerImpl()) : mCardModelFavoriteClickListenerOnFavoriteClickedAndroidViewViewOnClickListener).setValue(cardModelFavoriteClickListener));
                }
                if (cardModelCardClickListener != null) {
                    // read cardModel.cardClickListener::onCardClicked
                    cardModelCardClickListenerOnCardClickedAndroidViewViewOnClickListener = (((mCardModelCardClickListenerOnCardClickedAndroidViewViewOnClickListener == null) ? (mCardModelCardClickListenerOnCardClickedAndroidViewViewOnClickListener = new OnClickListenerImpl1()) : mCardModelCardClickListenerOnCardClickedAndroidViewViewOnClickListener).setValue(cardModelCardClickListener));
                }
                if (cardModelCountryModel != null) {
                    // read cardModel.countryModel.name
                    cardModelCountryModelName = cardModelCountryModel.getName();
                    // read cardModel.countryModel.favorite
                    cardModelCountryModelFavorite = cardModelCountryModel.isFavorite();
                    // read cardModel.countryModel.region
                    cardModelCountryModelRegion = cardModelCountryModel.getRegion();
                    // read cardModel.countryModel.capital
                    cardModelCountryModelCapital = cardModelCountryModel.getCapital();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(cardModelCountryModelFavorite) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }


                // read cardModel.countryModel.favorite ? @android:drawable/ic_favorite_filled : @android:drawable/ic_favorite_outline
                cardModelCountryModelFavoriteImFavoriteIconAndroidDrawableIcFavoriteFilledImFavoriteIconAndroidDrawableIcFavoriteOutline = ((cardModelCountryModelFavorite) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(imFavoriteIcon.getContext(), R.drawable.ic_favorite_filled)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(imFavoriteIcon.getContext(), R.drawable.ic_favorite_outline)));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.imFavoriteIcon.setOnClickListener(cardModelFavoriteClickListenerOnFavoriteClickedAndroidViewViewOnClickListener);
            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.imFavoriteIcon, cardModelCountryModelFavoriteImFavoriteIconAndroidDrawableIcFavoriteFilledImFavoriteIconAndroidDrawableIcFavoriteOutline);
            this.imFavoriteIcon.setTag(cardModel);
            this.mboundView0.setOnClickListener(cardModelCardClickListenerOnCardClickedAndroidViewViewOnClickListener);
            this.mboundView0.setTag(cardModel);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, cardModelCountryModelCapital);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, cardModelCountryModelRegion);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView6, cardModelCountryModelName);
        }
    }
    // Listener Stub Implementations
    public static class OnClickListenerImpl implements android.view.View.OnClickListener{
        private ie.letsgetcheckedtask.supportClasses.interfaces.IFavoriteAction value;
        public OnClickListenerImpl setValue(ie.letsgetcheckedtask.supportClasses.interfaces.IFavoriteAction value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onClick(android.view.View arg0) {
            this.value.onFavoriteClicked(arg0); 
        }
    }
    public static class OnClickListenerImpl1 implements android.view.View.OnClickListener{
        private ie.letsgetcheckedtask.supportClasses.interfaces.ICardClickListener value;
        public OnClickListenerImpl1 setValue(ie.letsgetcheckedtask.supportClasses.interfaces.ICardClickListener value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onClick(android.view.View arg0) {
            this.value.onCardClicked(arg0); 
        }
    }
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): cardModel
        flag 1 (0x2L): null
        flag 2 (0x3L): cardModel.countryModel.favorite ? @android:drawable/ic_favorite_filled : @android:drawable/ic_favorite_outline
        flag 3 (0x4L): cardModel.countryModel.favorite ? @android:drawable/ic_favorite_filled : @android:drawable/ic_favorite_outline
    flag mapping end*/
    //end
}
// Generated by data binding compiler. Do not edit!
package ie.letsgetcheckedtask.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.button.MaterialButton;
import ie.letsgetcheckedtask.R;
import ie.letsgetcheckedtask.models.binderModels.DialogCountryBinderModel;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class DialogCountryDetailsBinding extends ViewDataBinding {
  @NonNull
  public final Button btnDismiss;

  @NonNull
  public final MaterialButton btnFavorite;

  @NonNull
  public final LinearLayout linearLayout;

  @NonNull
  public final LinearLayout linearLayout2;

  @NonNull
  public final TextView textView5;

  @Bindable
  protected DialogCountryBinderModel mDialogBinder;

  protected DialogCountryDetailsBinding(Object _bindingComponent, View _root, int _localFieldCount,
      Button btnDismiss, MaterialButton btnFavorite, LinearLayout linearLayout,
      LinearLayout linearLayout2, TextView textView5) {
    super(_bindingComponent, _root, _localFieldCount);
    this.btnDismiss = btnDismiss;
    this.btnFavorite = btnFavorite;
    this.linearLayout = linearLayout;
    this.linearLayout2 = linearLayout2;
    this.textView5 = textView5;
  }

  public abstract void setDialogBinder(@Nullable DialogCountryBinderModel dialogBinder);

  @Nullable
  public DialogCountryBinderModel getDialogBinder() {
    return mDialogBinder;
  }

  @NonNull
  public static DialogCountryDetailsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.dialog_country_details, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static DialogCountryDetailsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<DialogCountryDetailsBinding>inflateInternal(inflater, R.layout.dialog_country_details, root, attachToRoot, component);
  }

  @NonNull
  public static DialogCountryDetailsBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.dialog_country_details, null, false, component)
   */
  @NonNull
  @Deprecated
  public static DialogCountryDetailsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<DialogCountryDetailsBinding>inflateInternal(inflater, R.layout.dialog_country_details, null, false, component);
  }

  public static DialogCountryDetailsBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static DialogCountryDetailsBinding bind(@NonNull View view, @Nullable Object component) {
    return (DialogCountryDetailsBinding)bind(component, view, R.layout.dialog_country_details);
  }
}

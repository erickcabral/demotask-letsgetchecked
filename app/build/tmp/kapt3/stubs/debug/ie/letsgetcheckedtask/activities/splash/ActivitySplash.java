package ie.letsgetcheckedtask.activities.splash;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0011\u001a\u00020\u0012H\u0002J\b\u0010\u0013\u001a\u00020\u0012H\u0002J\u0012\u0010\u0014\u001a\u00020\u00122\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0014J\b\u0010\u0017\u001a\u00020\u0012H\u0002J\b\u0010\u0018\u001a\u00020\u0012H\u0014J\b\u0010\u0019\u001a\u00020\u0012H\u0002J\b\u0010\u001a\u001a\u00020\u0012H\u0002J\b\u0010\u001b\u001a\u00020\u0012H\u0002J\b\u0010\u001c\u001a\u00020\u0012H\u0002J\b\u0010\u001d\u001a\u00020\u0012H\u0002J\b\u0010\u001e\u001a\u00020\u0012H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"}, d2 = {"Lie/letsgetcheckedtask/activities/splash/ActivitySplash;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "TAG", "", "connManager", "Landroid/net/ConnectivityManager;", "fadeIn", "Landroid/view/animation/Animation;", "fadeIn2", "fadeIn3", "fadeIn4", "fadeIn5", "networkCapabilities", "Landroid/net/NetworkCapabilities;", "viewModel", "Lie/letsgetcheckedtask/activities/splash/ActivitySplashViewModel;", "displayAlert", "", "onCountriesListRetrieved", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onRetrievingError", "onStart", "onUpdatedListRetrieved", "startAnim1", "startAnim2", "startAnim3", "startAnim4", "startAnim5", "app_debug"})
public final class ActivitySplash extends androidx.appcompat.app.AppCompatActivity {
    private final java.lang.String TAG = "<<_ACT_SPLASH_>>";
    private ie.letsgetcheckedtask.activities.splash.ActivitySplashViewModel viewModel;
    private android.view.animation.Animation fadeIn;
    private android.view.animation.Animation fadeIn2;
    private android.view.animation.Animation fadeIn3;
    private android.view.animation.Animation fadeIn4;
    private android.view.animation.Animation fadeIn5;
    private android.net.ConnectivityManager connManager;
    private android.net.NetworkCapabilities networkCapabilities;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    private final void onRetrievingError() {
    }
    
    private final void onCountriesListRetrieved() {
    }
    
    private final void onUpdatedListRetrieved() {
    }
    
    private final void startAnim1() {
    }
    
    private final void startAnim2() {
    }
    
    private final void startAnim3() {
    }
    
    private final void startAnim4() {
    }
    
    private final void startAnim5() {
    }
    
    private final void displayAlert() {
    }
    
    public ActivitySplash() {
        super();
    }
}
package ie.letsgetcheckedtask.models.adapterModel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001b\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\f"}, d2 = {"Lie/letsgetcheckedtask/models/adapterModel/AdapterCountriesModel;", "", "countriesList", "", "Lie/letsgetcheckedtask/models/CountryModel;", "parentFragment", "Landroidx/fragment/app/Fragment;", "(Ljava/util/List;Landroidx/fragment/app/Fragment;)V", "getCountriesList", "()Ljava/util/List;", "getParentFragment", "()Landroidx/fragment/app/Fragment;", "app_debug"})
public final class AdapterCountriesModel {
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<ie.letsgetcheckedtask.models.CountryModel> countriesList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.fragment.app.Fragment parentFragment = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<ie.letsgetcheckedtask.models.CountryModel> getCountriesList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.fragment.app.Fragment getParentFragment() {
        return null;
    }
    
    public AdapterCountriesModel(@org.jetbrains.annotations.NotNull()
    java.util.List<ie.letsgetcheckedtask.models.CountryModel> countriesList, @org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment parentFragment) {
        super();
    }
}
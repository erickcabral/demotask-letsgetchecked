package ie.letsgetcheckedtask.supportClasses.repositories;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\u0014\u001a\u00020\u0015J\u0006\u0010\u0016\u001a\u00020\u0015J\u0012\u0010\u0017\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\u0018J\u0012\u0010\u0019\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\u001aJ\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\b0\u001aR\u000e\u0010\u0007\u001a\u00020\bX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\b0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013\u00a8\u0006\u001c"}, d2 = {"Lie/letsgetcheckedtask/supportClasses/repositories/CountriesRepository;", "", "rapidAPI", "Lie/letsgetcheckedtask/supportClasses/interfaces/RapidAPI;", "defaultDispatcher", "Lkotlinx/coroutines/CoroutineDispatcher;", "(Lie/letsgetcheckedtask/supportClasses/interfaces/RapidAPI;Lkotlinx/coroutines/CoroutineDispatcher;)V", "TAG", "", "coroutineException", "Lkotlinx/coroutines/CoroutineExceptionHandler;", "getDefaultDispatcher", "()Lkotlinx/coroutines/CoroutineDispatcher;", "lvdCountriesListResponse", "Landroidx/lifecycle/MutableLiveData;", "", "Lie/letsgetcheckedtask/models/CountryModel;", "lvdOnListError", "getRapidAPI", "()Lie/letsgetcheckedtask/supportClasses/interfaces/RapidAPI;", "clear", "", "fetchCountries", "getCountriesListResponse", "Lkotlinx/coroutines/flow/Flow;", "getCountriesListResponseList", "Landroidx/lifecycle/LiveData;", "getListErrorStatus", "app_debug"})
public final class CountriesRepository {
    private final java.lang.String TAG = "<<_REPO_RAPID_>>";
    private final kotlinx.coroutines.CoroutineExceptionHandler coroutineException = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<ie.letsgetcheckedtask.models.CountryModel>> lvdCountriesListResponse = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.String> lvdOnListError = null;
    @org.jetbrains.annotations.NotNull()
    private final ie.letsgetcheckedtask.supportClasses.interfaces.RapidAPI rapidAPI = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlinx.coroutines.CoroutineDispatcher defaultDispatcher = null;
    
    public final void fetchCountries() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<ie.letsgetcheckedtask.models.CountryModel>> getCountriesListResponseList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<java.util.List<ie.letsgetcheckedtask.models.CountryModel>> getCountriesListResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.String> getListErrorStatus() {
        return null;
    }
    
    public final void clear() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final ie.letsgetcheckedtask.supportClasses.interfaces.RapidAPI getRapidAPI() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.CoroutineDispatcher getDefaultDispatcher() {
        return null;
    }
    
    public CountriesRepository(@org.jetbrains.annotations.NotNull()
    ie.letsgetcheckedtask.supportClasses.interfaces.RapidAPI rapidAPI, @org.jetbrains.annotations.NotNull()
    kotlinx.coroutines.CoroutineDispatcher defaultDispatcher) {
        super();
    }
}
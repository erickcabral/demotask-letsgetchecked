package ie.letsgetcheckedtask.models.binderModels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000f\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u001c\u0010\u000f\u001a\u0004\u0018\u00010\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\n\"\u0004\b\u0011\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u001a\u0010\u0014\u001a\u00020\u0015X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u0011\u0010\u001a\u001a\u00020\u001b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u001c\u0010\u001e\u001a\u0004\u0018\u00010\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\n\"\u0004\b \u0010\fR\u001c\u0010!\u001a\u0004\u0018\u00010\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\n\"\u0004\b#\u0010\fR\u001c\u0010$\u001a\u0004\u0018\u00010\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\n\"\u0004\b&\u0010\fR\u001c\u0010\'\u001a\u0004\u0018\u00010\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\n\"\u0004\b)\u0010\f\u00a8\u0006*"}, d2 = {"Lie/letsgetcheckedtask/models/binderModels/DialogCountryBinderModel;", "", "cardBinderModel", "Lie/letsgetcheckedtask/models/binderModels/CardBinderModel;", "dismissListener", "Landroid/view/View$OnClickListener;", "(Lie/letsgetcheckedtask/models/binderModels/CardBinderModel;Landroid/view/View$OnClickListener;)V", "capital", "", "getCapital", "()Ljava/lang/String;", "setCapital", "(Ljava/lang/String;)V", "getCardBinderModel", "()Lie/letsgetcheckedtask/models/binderModels/CardBinderModel;", "denomyn", "getDenomyn", "setDenomyn", "getDismissListener", "()Landroid/view/View$OnClickListener;", "favorite", "", "getFavorite", "()Z", "setFavorite", "(Z)V", "favoriteClickListener", "Lie/letsgetcheckedtask/supportClasses/interfaces/IFavoriteAction;", "getFavoriteClickListener", "()Lie/letsgetcheckedtask/supportClasses/interfaces/IFavoriteAction;", "name", "getName", "setName", "population", "getPopulation", "setPopulation", "region", "getRegion", "setRegion", "subRegion", "getSubRegion", "setSubRegion", "app_debug"})
public final class DialogCountryBinderModel {
    @org.jetbrains.annotations.Nullable()
    private java.lang.String name;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String denomyn;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String capital;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String region;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String subRegion;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String population;
    private boolean favorite;
    @org.jetbrains.annotations.NotNull()
    private final ie.letsgetcheckedtask.supportClasses.interfaces.IFavoriteAction favoriteClickListener = null;
    @org.jetbrains.annotations.NotNull()
    private final ie.letsgetcheckedtask.models.binderModels.CardBinderModel cardBinderModel = null;
    @org.jetbrains.annotations.NotNull()
    private final android.view.View.OnClickListener dismissListener = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getName() {
        return null;
    }
    
    public final void setName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDenomyn() {
        return null;
    }
    
    public final void setDenomyn(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCapital() {
        return null;
    }
    
    public final void setCapital(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRegion() {
        return null;
    }
    
    public final void setRegion(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSubRegion() {
        return null;
    }
    
    public final void setSubRegion(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPopulation() {
        return null;
    }
    
    public final void setPopulation(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public final boolean getFavorite() {
        return false;
    }
    
    public final void setFavorite(boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final ie.letsgetcheckedtask.supportClasses.interfaces.IFavoriteAction getFavoriteClickListener() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final ie.letsgetcheckedtask.models.binderModels.CardBinderModel getCardBinderModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View.OnClickListener getDismissListener() {
        return null;
    }
    
    public DialogCountryBinderModel(@org.jetbrains.annotations.NotNull()
    ie.letsgetcheckedtask.models.binderModels.CardBinderModel cardBinderModel, @org.jetbrains.annotations.NotNull()
    android.view.View.OnClickListener dismissListener) {
        super();
    }
}
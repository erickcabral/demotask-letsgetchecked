package ie.letsgetcheckedtask.supportClasses;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\b\u001a\u00020\tJ\u0006\u0010\n\u001a\u00020\tJ\u0018\u0010\u000b\u001a\u00020\t2\b\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u000fJ\u0018\u0010\u0007\u001a\u00020\t2\b\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\u0010\u001a\u00020\u0011J\u0018\u0010\u0012\u001a\u00020\t2\u0006\u0010\u0013\u001a\u00020\u00142\b\u0010\f\u001a\u0004\u0018\u00010\rR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lie/letsgetcheckedtask/supportClasses/OutputManager;", "", "()V", "DIALOG_BACKGROUND_COLOR", "", "dialogCountryDetails", "Landroidx/appcompat/app/AlertDialog;", "dialogPositiveOnly", "closeCountryDialog", "", "closePositiveOnlyDialog", "countryWindow", "parent_context", "Landroid/content/Context;", "cardBinderModel", "Lie/letsgetcheckedtask/models/binderModels/CardBinderModel;", "dialogPositiveModel", "Lie/letsgetcheckedtask/models/binderModels/DialogPositiveModel;", "toastShort", "message", "", "app_debug"})
public final class OutputManager {
    private static final int DIALOG_BACKGROUND_COLOR = android.R.color.transparent;
    private static androidx.appcompat.app.AlertDialog dialogCountryDetails;
    private static androidx.appcompat.app.AlertDialog dialogPositiveOnly;
    public static final ie.letsgetcheckedtask.supportClasses.OutputManager INSTANCE = null;
    
    public final void toastShort(@org.jetbrains.annotations.NotNull()
    java.lang.String message, @org.jetbrains.annotations.Nullable()
    android.content.Context parent_context) {
    }
    
    public final void countryWindow(@org.jetbrains.annotations.Nullable()
    android.content.Context parent_context, @org.jetbrains.annotations.NotNull()
    ie.letsgetcheckedtask.models.binderModels.CardBinderModel cardBinderModel) {
    }
    
    public final void closeCountryDialog() {
    }
    
    public final void dialogPositiveOnly(@org.jetbrains.annotations.Nullable()
    android.content.Context parent_context, @org.jetbrains.annotations.NotNull()
    ie.letsgetcheckedtask.models.binderModels.DialogPositiveModel dialogPositiveModel) {
    }
    
    public final void closePositiveOnlyDialog() {
    }
    
    private OutputManager() {
        super();
    }
}
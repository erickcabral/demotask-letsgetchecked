package ie.letsgetcheckedtask.fragViews;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u001a\u001a\u00020\u001bJ\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\r0\u001dJ\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\r0\u001dJ\u0006\u0010\u001f\u001a\u00020\u001bJ\u000e\u0010 \u001a\u00020\u001b2\u0006\u0010!\u001a\u00020\u0019J\u0006\u0010\"\u001a\u00020\u001bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0017\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\r0\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000fR\u001c\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"}, d2 = {"Lie/letsgetcheckedtask/fragViews/CountriesListViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "TAG", "", "itemPosition", "", "getItemPosition", "()I", "setItemPosition", "(I)V", "lvdFavoriteModel", "Landroidx/lifecycle/MutableLiveData;", "Lie/letsgetcheckedtask/models/CountryModel;", "getLvdFavoriteModel", "()Landroidx/lifecycle/MutableLiveData;", "lvdRemovedModel", "getLvdRemovedModel", "selectedItem", "Lie/letsgetcheckedtask/models/binderModels/CardBinderModel;", "getSelectedItem", "()Lie/letsgetcheckedtask/models/binderModels/CardBinderModel;", "setSelectedItem", "(Lie/letsgetcheckedtask/models/binderModels/CardBinderModel;)V", "viewSelected", "Landroid/view/View;", "clearCurrentFavorite", "", "getFavoritedModel", "Landroidx/lifecycle/LiveData;", "getRemovedModel", "setItemLocked", "setViewClicked", "view", "updateFavorite", "app_debug"})
public final class CountriesListViewModel extends androidx.lifecycle.ViewModel {
    private final java.lang.String TAG = "<<_COUNTRIES_LIST_VM_>>";
    private int itemPosition = -1;
    @org.jetbrains.annotations.Nullable()
    private ie.letsgetcheckedtask.models.binderModels.CardBinderModel selectedItem;
    private android.view.View viewSelected;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<ie.letsgetcheckedtask.models.CountryModel> lvdFavoriteModel = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<ie.letsgetcheckedtask.models.CountryModel> lvdRemovedModel = null;
    
    public final int getItemPosition() {
        return 0;
    }
    
    public final void setItemPosition(int p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final ie.letsgetcheckedtask.models.binderModels.CardBinderModel getSelectedItem() {
        return null;
    }
    
    public final void setSelectedItem(@org.jetbrains.annotations.Nullable()
    ie.letsgetcheckedtask.models.binderModels.CardBinderModel p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<ie.letsgetcheckedtask.models.CountryModel> getLvdFavoriteModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<ie.letsgetcheckedtask.models.CountryModel> getLvdRemovedModel() {
        return null;
    }
    
    public final void updateFavorite() {
    }
    
    public final void clearCurrentFavorite() {
    }
    
    public final void setViewClicked(@org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
    
    public final void setItemLocked() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<ie.letsgetcheckedtask.models.CountryModel> getFavoritedModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<ie.letsgetcheckedtask.models.CountryModel> getRemovedModel() {
        return null;
    }
    
    public CountriesListViewModel() {
        super();
    }
}
package ie.letsgetcheckedtask.activities.splash;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\t\u001a\u00020\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\b0\u0007J\u0006\u0010\f\u001a\u00020\nJ\u0012\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u000eJ\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\u000eJ\u0012\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u000eJ\u000e\u0010\u0012\u001a\u00020\n2\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u0017J\u0006\u0010\u0018\u001a\u00020\nR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"}, d2 = {"Lie/letsgetcheckedtask/activities/splash/ActivitySplashViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "countriesRepository", "Lie/letsgetcheckedtask/supportClasses/repositories/CountriesRepository;", "lvdUpdatedList", "Landroidx/lifecycle/MutableLiveData;", "", "Lie/letsgetcheckedtask/models/CountryModel;", "checkFavorites", "", "list", "clearRepository", "getCountriesListResponse", "Landroidx/lifecycle/LiveData;", "getListErrorStatus", "", "getUpdatedList", "initializeRepository", "rapidAPI", "Lie/letsgetcheckedtask/supportClasses/interfaces/RapidAPI;", "openCountryList", "activity", "Landroidx/appcompat/app/AppCompatActivity;", "retrieveCountries", "app_debug"})
public final class ActivitySplashViewModel extends androidx.lifecycle.ViewModel {
    private ie.letsgetcheckedtask.supportClasses.repositories.CountriesRepository countriesRepository;
    private final androidx.lifecycle.MutableLiveData<java.util.List<ie.letsgetcheckedtask.models.CountryModel>> lvdUpdatedList = null;
    
    public final void initializeRepository(@org.jetbrains.annotations.NotNull()
    ie.letsgetcheckedtask.supportClasses.interfaces.RapidAPI rapidAPI) {
    }
    
    public final void retrieveCountries() {
    }
    
    public final void openCountryList(@org.jetbrains.annotations.NotNull()
    androidx.appcompat.app.AppCompatActivity activity) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<ie.letsgetcheckedtask.models.CountryModel>> getCountriesListResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<ie.letsgetcheckedtask.models.CountryModel>> getUpdatedList() {
        return null;
    }
    
    public final void checkFavorites(@org.jetbrains.annotations.NotNull()
    java.util.List<ie.letsgetcheckedtask.models.CountryModel> list) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.String> getListErrorStatus() {
        return null;
    }
    
    public final void clearRepository() {
    }
    
    public ActivitySplashViewModel() {
        super();
    }
}
package ie.letsgetcheckedtask.supportClasses;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\"\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0018\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0007\u001a\u001a\u0010\u0006\u001a\u00020\u00012\u0006\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0007\u00a8\u0006\u000b"}, d2 = {"formatPopulation", "", "textView", "Landroid/widget/TextView;", "number", "", "loadRecycler", "recycler", "Landroidx/recyclerview/widget/RecyclerView;", "adapterCountriesModel", "Lie/letsgetcheckedtask/models/adapterModel/AdapterCountriesModel;", "app_debug"})
public final class BinderAdaptersKt {
    
    @androidx.databinding.BindingAdapter(value = {"android:adapterCountries"})
    public static final void loadRecycler(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView recycler, @org.jetbrains.annotations.Nullable()
    ie.letsgetcheckedtask.models.adapterModel.AdapterCountriesModel adapterCountriesModel) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"android:formatPopulation"})
    public static final void formatPopulation(@org.jetbrains.annotations.NotNull()
    android.widget.TextView textView, @org.jetbrains.annotations.NotNull()
    java.lang.String number) {
    }
}
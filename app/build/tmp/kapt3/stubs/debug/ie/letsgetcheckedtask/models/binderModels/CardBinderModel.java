package ie.letsgetcheckedtask.models.binderModels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0007H\u00c2\u0003J\'\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010 \u001a\u00020\u0005H\u00d6\u0001J\t\u0010!\u001a\u00020\"H\u00d6\u0001R\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u00020\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"}, d2 = {"Lie/letsgetcheckedtask/models/binderModels/CardBinderModel;", "", "countryModel", "Lie/letsgetcheckedtask/models/CountryModel;", "listPosition", "", "parent_fragment", "Landroidx/fragment/app/Fragment;", "(Lie/letsgetcheckedtask/models/CountryModel;ILandroidx/fragment/app/Fragment;)V", "cardClickListener", "Lie/letsgetcheckedtask/supportClasses/interfaces/ICardClickListener;", "getCardClickListener", "()Lie/letsgetcheckedtask/supportClasses/interfaces/ICardClickListener;", "setCardClickListener", "(Lie/letsgetcheckedtask/supportClasses/interfaces/ICardClickListener;)V", "getCountryModel", "()Lie/letsgetcheckedtask/models/CountryModel;", "favoriteClickListener", "Lie/letsgetcheckedtask/supportClasses/interfaces/IFavoriteAction;", "getFavoriteClickListener", "()Lie/letsgetcheckedtask/supportClasses/interfaces/IFavoriteAction;", "setFavoriteClickListener", "(Lie/letsgetcheckedtask/supportClasses/interfaces/IFavoriteAction;)V", "getListPosition", "()I", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "toString", "", "app_debug"})
public final class CardBinderModel {
    @org.jetbrains.annotations.NotNull()
    private ie.letsgetcheckedtask.supportClasses.interfaces.IFavoriteAction favoriteClickListener;
    @org.jetbrains.annotations.NotNull()
    private ie.letsgetcheckedtask.supportClasses.interfaces.ICardClickListener cardClickListener;
    @org.jetbrains.annotations.NotNull()
    private final ie.letsgetcheckedtask.models.CountryModel countryModel = null;
    private final int listPosition = 0;
    private final androidx.fragment.app.Fragment parent_fragment = null;
    
    @org.jetbrains.annotations.NotNull()
    public final ie.letsgetcheckedtask.supportClasses.interfaces.IFavoriteAction getFavoriteClickListener() {
        return null;
    }
    
    public final void setFavoriteClickListener(@org.jetbrains.annotations.NotNull()
    ie.letsgetcheckedtask.supportClasses.interfaces.IFavoriteAction p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final ie.letsgetcheckedtask.supportClasses.interfaces.ICardClickListener getCardClickListener() {
        return null;
    }
    
    public final void setCardClickListener(@org.jetbrains.annotations.NotNull()
    ie.letsgetcheckedtask.supportClasses.interfaces.ICardClickListener p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final ie.letsgetcheckedtask.models.CountryModel getCountryModel() {
        return null;
    }
    
    public final int getListPosition() {
        return 0;
    }
    
    public CardBinderModel(@org.jetbrains.annotations.NotNull()
    ie.letsgetcheckedtask.models.CountryModel countryModel, int listPosition, @org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment parent_fragment) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final ie.letsgetcheckedtask.models.CountryModel component1() {
        return null;
    }
    
    public final int component2() {
        return 0;
    }
    
    private final androidx.fragment.app.Fragment component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final ie.letsgetcheckedtask.models.binderModels.CardBinderModel copy(@org.jetbrains.annotations.NotNull()
    ie.letsgetcheckedtask.models.CountryModel countryModel, int listPosition, @org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment parent_fragment) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}
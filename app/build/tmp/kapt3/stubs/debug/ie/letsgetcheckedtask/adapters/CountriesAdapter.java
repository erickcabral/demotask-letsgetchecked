package ie.letsgetcheckedtask.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0017B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\b\u0010\r\u001a\u00020\u000eH\u0016J\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u000eH\u0016J\u0018\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u000eH\u0016R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\u0018"}, d2 = {"Lie/letsgetcheckedtask/adapters/CountriesAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lie/letsgetcheckedtask/adapters/CountriesAdapter$CountriesViewHolder;", "adapterModel", "Lie/letsgetcheckedtask/models/adapterModel/AdapterCountriesModel;", "(Lie/letsgetcheckedtask/models/adapterModel/AdapterCountriesModel;)V", "getAdapterModel", "()Lie/letsgetcheckedtask/models/adapterModel/AdapterCountriesModel;", "list", "", "Lie/letsgetcheckedtask/models/CountryModel;", "getList", "()Ljava/util/List;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "CountriesViewHolder", "app_debug"})
public final class CountriesAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<ie.letsgetcheckedtask.adapters.CountriesAdapter.CountriesViewHolder> {
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<ie.letsgetcheckedtask.models.CountryModel> list = null;
    @org.jetbrains.annotations.NotNull()
    private final ie.letsgetcheckedtask.models.adapterModel.AdapterCountriesModel adapterModel = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<ie.letsgetcheckedtask.models.CountryModel> getList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public ie.letsgetcheckedtask.adapters.CountriesAdapter.CountriesViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    ie.letsgetcheckedtask.adapters.CountriesAdapter.CountriesViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final ie.letsgetcheckedtask.models.adapterModel.AdapterCountriesModel getAdapterModel() {
        return null;
    }
    
    public CountriesAdapter(@org.jetbrains.annotations.NotNull()
    ie.letsgetcheckedtask.models.adapterModel.AdapterCountriesModel adapterModel) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lie/letsgetcheckedtask/adapters/CountriesAdapter$CountriesViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "binder", "Lie/letsgetcheckedtask/databinding/CardCountriesBinding;", "(Lie/letsgetcheckedtask/databinding/CardCountriesBinding;)V", "getBinder", "()Lie/letsgetcheckedtask/databinding/CardCountriesBinding;", "app_debug"})
    public static final class CountriesViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final ie.letsgetcheckedtask.databinding.CardCountriesBinding binder = null;
        
        @org.jetbrains.annotations.NotNull()
        public final ie.letsgetcheckedtask.databinding.CardCountriesBinding getBinder() {
            return null;
        }
        
        public CountriesViewHolder(@org.jetbrains.annotations.NotNull()
        ie.letsgetcheckedtask.databinding.CardCountriesBinding binder) {
            super(null);
        }
    }
}
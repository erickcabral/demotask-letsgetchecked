package ie.letsgetcheckedtask.fragViews;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010 \n\u0002\b\u0003\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J&\u0010\u0014\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\b\u0010\u001b\u001a\u00020\u000eH\u0016J\u0010\u0010\u001c\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\b\u0010\u001d\u001a\u00020\u000eH\u0002J\b\u0010\u001e\u001a\u00020\u000eH\u0002J\u001a\u0010\u001f\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u00132\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\u0014\u0010 \u001a\u00020\u000e2\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00100\"J\u000e\u0010#\u001a\u00020\u000e2\u0006\u0010$\u001a\u00020\u0010R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0007\u001a\u00020\b8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\t\u0010\n\u00a8\u0006%"}, d2 = {"Lie/letsgetcheckedtask/fragViews/CountriesListView;", "Landroidx/fragment/app/Fragment;", "Lie/letsgetcheckedtask/supportClasses/interfaces/IFavoriteAction;", "Lie/letsgetcheckedtask/supportClasses/interfaces/ICardClickListener;", "()V", "viewBinder", "Lie/letsgetcheckedtask/databinding/FragViewCountriesListBinding;", "viewModel", "Lie/letsgetcheckedtask/fragViews/CountriesListViewModel;", "getViewModel", "()Lie/letsgetcheckedtask/fragViews/CountriesListViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "displayUpdateDialog", "", "countryModel", "Lie/letsgetcheckedtask/models/CountryModel;", "onCardClicked", "view", "Landroid/view/View;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onDismiss", "onFavoriteClicked", "onFavoriteRemoved", "onFavoriteUpdated", "onViewCreated", "setViewBinder", "countriesList", "", "updateRecyclerItem", "model", "app_debug"})
public final class CountriesListView extends androidx.fragment.app.Fragment implements ie.letsgetcheckedtask.supportClasses.interfaces.IFavoriteAction, ie.letsgetcheckedtask.supportClasses.interfaces.ICardClickListener {
    private ie.letsgetcheckedtask.databinding.FragViewCountriesListBinding viewBinder;
    private final kotlin.Lazy viewModel$delegate = null;
    private java.util.HashMap _$_findViewCache;
    
    private final ie.letsgetcheckedtask.fragViews.CountriesListViewModel getViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void onFavoriteUpdated() {
    }
    
    private final void onFavoriteRemoved() {
    }
    
    public final void displayUpdateDialog(@org.jetbrains.annotations.NotNull()
    ie.letsgetcheckedtask.models.CountryModel countryModel) {
    }
    
    public final void setViewBinder(@org.jetbrains.annotations.NotNull()
    java.util.List<ie.letsgetcheckedtask.models.CountryModel> countriesList) {
    }
    
    public final void updateRecyclerItem(@org.jetbrains.annotations.NotNull()
    ie.letsgetcheckedtask.models.CountryModel model) {
    }
    
    @java.lang.Override()
    public void onFavoriteClicked(@org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
    
    @java.lang.Override()
    public void onCardClicked(@org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
    
    @java.lang.Override()
    public void onDismiss() {
    }
    
    public CountriesListView() {
        super();
    }
}
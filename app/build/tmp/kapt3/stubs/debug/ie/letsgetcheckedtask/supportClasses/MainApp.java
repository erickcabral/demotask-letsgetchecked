package ie.letsgetcheckedtask.supportClasses;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u000f\u001a\u00020\u0004J\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\f0\u000bJ\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004J\u0006\u0010\u0012\u001a\u00020\u000eJ\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0007J\u0006\u0010\u0018\u001a\u00020\u0016J\u0014\u0010\u0019\u001a\u00020\u00162\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\f0\u000bJ\u0010\u0010\u001b\u001a\u00020\u00162\b\u0010\u001c\u001a\u0004\u0018\u00010\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"}, d2 = {"Lie/letsgetcheckedtask/supportClasses/MainApp;", "", "()V", "FAV_COUNTRY", "", "SHARED_PREFS_TAG", "appApplication", "Landroid/app/Application;", "appSharedPreferences", "Landroid/content/SharedPreferences;", "countriesList", "", "Lie/letsgetcheckedtask/models/CountryModel;", "rapidAPI", "Lie/letsgetcheckedtask/supportClasses/interfaces/RapidAPI;", "getApiKey", "getCountriesList", "getFavoriteCountry", "getRapidAPI", "hasFavorite", "", "initialize", "", "application", "removeFavoriteCountry", "setCountriesList", "list", "setFavoriteCountry", "name", "app_debug"})
public final class MainApp {
    private static final java.lang.String SHARED_PREFS_TAG = "SharedPrefs";
    private static final java.lang.String FAV_COUNTRY = "CountryID";
    private static ie.letsgetcheckedtask.supportClasses.interfaces.RapidAPI rapidAPI;
    private static android.app.Application appApplication;
    private static android.content.SharedPreferences appSharedPreferences;
    private static java.util.List<ie.letsgetcheckedtask.models.CountryModel> countriesList;
    public static final ie.letsgetcheckedtask.supportClasses.MainApp INSTANCE = null;
    
    public final void initialize(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getApiKey() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final ie.letsgetcheckedtask.supportClasses.interfaces.RapidAPI getRapidAPI() {
        return null;
    }
    
    public final void setFavoriteCountry(@org.jetbrains.annotations.Nullable()
    java.lang.String name) {
    }
    
    public final boolean hasFavorite() {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getFavoriteCountry() {
        return null;
    }
    
    public final void removeFavoriteCountry() {
    }
    
    public final void setCountriesList(@org.jetbrains.annotations.NotNull()
    java.util.List<ie.letsgetcheckedtask.models.CountryModel> list) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<ie.letsgetcheckedtask.models.CountryModel> getCountriesList() {
        return null;
    }
    
    private MainApp() {
        super();
    }
}
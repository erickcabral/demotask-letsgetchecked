package ie.letsgetcheckedtask.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ie.letsgetcheckedtask.R

class ActivityMain : AppCompatActivity() {
    private val TAG = "<<_ACT_MAIN_>>"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
package ie.letsgetcheckedtask.activities.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ie.letsgetcheckedtask.activities.ActivityMain
import ie.letsgetcheckedtask.models.CountryModel
import ie.letsgetcheckedtask.supportClasses.MainApp
import ie.letsgetcheckedtask.supportClasses.interfaces.RapidAPI
import ie.letsgetcheckedtask.supportClasses.repositories.CountriesRepository
import kotlinx.coroutines.Dispatchers

class ActivitySplashViewModel : ViewModel() {
    private lateinit var countriesRepository: CountriesRepository

    fun initializeRepository(rapidAPI: RapidAPI) {
        this.countriesRepository = CountriesRepository(rapidAPI, Dispatchers.IO)
    }

    private val lvdUpdatedList: MutableLiveData<List<CountryModel>> =
        MutableLiveData<List<CountryModel>>()
//
//    fun checkConnection(networkCapabilities: NetworkCapabilities?) {
//        this.countriesRepository.checkConnection(networkCapabilities)
//    }

    fun retrieveCountries() {
        this.countriesRepository.fetchCountries()
    }

    fun openCountryList(activity: AppCompatActivity) {
        activity.startActivity(Intent(activity, ActivityMain::class.java))
        activity.finish()
    }

    /*======================== GETTERS ====================================*/
    fun getCountriesListResponse(): LiveData<List<CountryModel>> {
        return this.countriesRepository.getCountriesListResponseList()
    }

//    fun getCountriesListResponse(): LiveData<List<CountryModel>> {
//        return this.countriesRepository.getCountriesListResponse().asLiveData()
//    }

    fun getUpdatedList(): LiveData<List<CountryModel>> {
        return lvdUpdatedList
    }

    fun checkFavorites(list: List<CountryModel>) {
        if (MainApp.hasFavorite()) {
            val favoriteName = MainApp.getFavoriteCountry()
            for (model in list) {
                if (model.name == favoriteName) {
                    model.isFavorite = true
                    break
                }
            }
        }
        this.lvdUpdatedList.postValue(list)
    }

    fun getListErrorStatus(): LiveData<String> {
        return this.countriesRepository.getListErrorStatus()
    }

    fun clearRepository(){
        this.countriesRepository.clear()
    }
}
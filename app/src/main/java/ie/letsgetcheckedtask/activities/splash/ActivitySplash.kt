package ie.letsgetcheckedtask.activities.splash

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import ie.letsgetcheckedtask.R
import ie.letsgetcheckedtask.models.CountryModel
import ie.letsgetcheckedtask.models.binderModels.DialogPositiveModel
import ie.letsgetcheckedtask.supportClasses.MainApp
import ie.letsgetcheckedtask.supportClasses.OutputManager
import kotlinx.android.synthetic.main.activity_splash.*

class ActivitySplash : AppCompatActivity() {
    private val TAG = "<<_ACT_SPLASH_>>"

    private lateinit var viewModel: ActivitySplashViewModel

    private lateinit var fadeIn: Animation
    private lateinit var fadeIn2: Animation
    private lateinit var fadeIn3: Animation
    private lateinit var fadeIn4: Animation
    private lateinit var fadeIn5: Animation


    private lateinit var connManager: ConnectivityManager
    private var networkCapabilities: NetworkCapabilities? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        MainApp.initialize(application)

        this.connManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager



        this.fadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in)
        fadeIn.duration = 500L
        this.fadeIn2 = AnimationUtils.loadAnimation(this, R.anim.fade_in)
        this.fadeIn2.duration = 500L
        this.fadeIn3 = AnimationUtils.loadAnimation(this, R.anim.fade_in)
        fadeIn3.duration = 500L

        this.fadeIn4 = AnimationUtils.loadAnimation(this, R.anim.fade_in)
        fadeIn4.duration = 600L

        this.fadeIn5 = AnimationUtils.loadAnimation(this, R.anim.fade_in)
        fadeIn5.duration = 500L

        this.viewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(application)
            .create(ActivitySplashViewModel::class.java)
        this.viewModel.initializeRepository(MainApp.getRapidAPI())

        this.onCountriesListRetrieved()
        this.onRetrievingError()
        this.onUpdatedListRetrieved()
    }


    override fun onStart() {
        super.onStart()
        this.networkCapabilities = connManager.getNetworkCapabilities(connManager.activeNetwork)
        if (MainApp.getCountriesList().isEmpty()) {
            val isConnected = (networkCapabilities != null && networkCapabilities!!.hasCapability(
                NetworkCapabilities.NET_CAPABILITY_INTERNET
            ))
            if (isConnected) {
                startAnim1()
            } else {
                displayAlert()
            }
        } else {
            this.viewModel.openCountryList(this)
        }
    }

    private fun onRetrievingError() {
        this.viewModel.getListErrorStatus().observe(this, Observer { message ->
            message?.let {
                if (!message.isEmpty()) {
                    val alert_message = String.format("%s \n\n Please try again", message)
                    val dialogPositiveModel =
                        DialogPositiveModel(getString(R.string.txt_server_error), alert_message)
                    val buttonText = getString(R.string.txt_close_app)
                    val buttonListener = View.OnClickListener {
                        OutputManager.closePositiveOnlyDialog()
                        finish()
                    }
                    dialogPositiveModel.clickListener = buttonListener
                    dialogPositiveModel.buttonText = buttonText
                    OutputManager.dialogPositiveOnly(this, dialogPositiveModel)
                }
            }
        })
    }

    private fun onCountriesListRetrieved() {
        this.viewModel.getCountriesListResponse().let { liveData ->
            liveData.observe(this, Observer<List<CountryModel>> {
                if (it != null) {
                    Log.d(TAG, "LIST Countries -> ${it.size}")
                    viewModel.checkFavorites(it)
                }
            })
        }
    }

    private fun onUpdatedListRetrieved() {
        this.viewModel.getUpdatedList().let { updatedList ->
            updatedList.observe(this, Observer {
                if (it != null) {
                    MainApp.setCountriesList(it)
                    viewModel.openCountryList(this)
                }
            })
        }
    }

    private fun startAnim1() {
        tvLets.visibility = View.VISIBLE
        tvLets.startAnimation(fadeIn)
        tvLets.animate().withEndAction {
            startAnim2()
        }.apply {
            duration = 500L
        }.start()
    }

    private fun startAnim2() {
        tvGet.visibility = View.VISIBLE
        tvGet.startAnimation(fadeIn2)
        tvGet.animate().withEndAction {
            this.startAnim3()
        }.apply {
            duration = 500L
        }.start()
    }

    private fun startAnim3() {
        tvChecked.visibility = View.VISIBLE
        tvChecked.startAnimation(fadeIn3)
        tvChecked.animate().withEndAction {
            this.startAnim4()
        }.apply {
            duration = 500L
        }.start()
    }

    private fun startAnim4() {
        tvDemoTask.visibility = View.VISIBLE
        tvDemoTask.startAnimation(fadeIn4)
        tvDemoTask.animate().withEndAction {
            startAnim5()
        }.apply {
            duration = 500L
        }.start()

    }

    private fun startAnim5() {
        linByErick.visibility = View.VISIBLE
        linByErick.startAnimation(fadeIn5)
        tvDemoTask.animate().withEndAction {
            progressBar.visibility = View.VISIBLE
            this.viewModel.retrieveCountries()
        }.apply {
            duration = 1000L
        }.start()

    }

    private fun displayAlert() {
        val networkDialogModel = DialogPositiveModel(
            "No internet connection detected",
            "Please check your internet connection and try again"
        )

        networkDialogModel.buttonText = "Close App"
        networkDialogModel.clickListener = View.OnClickListener {
            OutputManager.closePositiveOnlyDialog()
            this.finish()
        }
        OutputManager.dialogPositiveOnly(this, networkDialogModel)
    }
}

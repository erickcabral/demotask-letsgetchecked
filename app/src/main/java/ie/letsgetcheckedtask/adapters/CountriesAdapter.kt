package ie.letsgetcheckedtask.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ie.letsgetcheckedtask.R
import ie.letsgetcheckedtask.databinding.CardCountriesBinding
import ie.letsgetcheckedtask.models.adapterModel.AdapterCountriesModel
import ie.letsgetcheckedtask.models.binderModels.CardBinderModel

class CountriesAdapter(val adapterModel: AdapterCountriesModel) :
    RecyclerView.Adapter<CountriesAdapter.CountriesViewHolder>() {

    val list = adapterModel.countriesList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountriesViewHolder {
        val binder = DataBindingUtil.inflate<CardCountriesBinding>(
            LayoutInflater.from(parent.context),
            R.layout.card_countries,
            parent,
            false
        )

        return CountriesViewHolder(binder)
    }

    override fun onBindViewHolder(holder: CountriesViewHolder, position: Int) {
        val cardTagModel =
            CardBinderModel(list.get(position), position, adapterModel.parentFragment)
        holder.binder.cardModel = cardTagModel
    }

    override fun getItemCount(): Int {
        return adapterModel.countriesList.size
    }


    class CountriesViewHolder(binder: CardCountriesBinding) : RecyclerView.ViewHolder(binder.root) {
        val binder: CardCountriesBinding = binder
    }

}
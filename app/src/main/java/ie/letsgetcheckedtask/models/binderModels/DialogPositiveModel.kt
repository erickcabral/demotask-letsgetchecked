package ie.letsgetcheckedtask.models.binderModels

import android.view.View

class DialogPositiveModel(title: String, message: String) {
    val title = title
    val message = message
    var buttonText:String? = null
    var clickListener: View.OnClickListener? =null
}
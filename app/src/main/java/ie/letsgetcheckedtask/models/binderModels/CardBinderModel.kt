package ie.letsgetcheckedtask.models.binderModels

import androidx.fragment.app.Fragment
import ie.letsgetcheckedtask.models.CountryModel
import ie.letsgetcheckedtask.supportClasses.interfaces.ICardClickListener
import ie.letsgetcheckedtask.supportClasses.interfaces.IFavoriteAction

data class CardBinderModel(
    val countryModel: CountryModel,
    val listPosition:Int,
    private val parent_fragment: Fragment
) {

    var favoriteClickListener: IFavoriteAction = parent_fragment as IFavoriteAction
    var cardClickListener: ICardClickListener = parent_fragment as ICardClickListener

}
package ie.letsgetcheckedtask.models.binderModels

import android.view.View

class DialogCountryBinderModel(
    val cardBinderModel: CardBinderModel,
    val dismissListener: View.OnClickListener
) {
    var name = cardBinderModel.countryModel.name
    var denomyn = cardBinderModel.countryModel.demonym
    var capital = cardBinderModel.countryModel.capital
    var region = cardBinderModel.countryModel.region
    var subRegion = cardBinderModel.countryModel.subRegion
    var population = cardBinderModel.countryModel.population
    var favorite = cardBinderModel.countryModel.isFavorite

    val favoriteClickListener = cardBinderModel.favoriteClickListener
}
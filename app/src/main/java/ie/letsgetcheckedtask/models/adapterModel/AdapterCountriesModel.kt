package ie.letsgetcheckedtask.models.adapterModel

import androidx.fragment.app.Fragment
import ie.letsgetcheckedtask.models.CountryModel

class AdapterCountriesModel(val countriesList:List<CountryModel>, val parentFragment:Fragment)
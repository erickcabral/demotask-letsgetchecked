package ie.letsgetcheckedtask.models

import com.google.gson.annotations.SerializedName

data class CountryModel(
    @SerializedName("name")
    var name: String? = "N/A",

    @SerializedName("nativename")
    var nativeName: String? = "N/A",

    @SerializedName("capital")
    var capital: String? = "N/A",

    @SerializedName("demonym")
    var demonym: String? = "N/A",

    @SerializedName("region")
    var region: String? = "N/A",

    @SerializedName("subregion")
    var subRegion: String? = "N/A",

    @SerializedName("population")
    var population: String? = "N/A",
) {
    var isFavorite: Boolean = false

}
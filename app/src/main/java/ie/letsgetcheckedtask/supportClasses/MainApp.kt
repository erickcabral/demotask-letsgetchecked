package ie.letsgetcheckedtask.supportClasses

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import ie.letsgetcheckedtask.R
import ie.letsgetcheckedtask.models.CountryModel
import ie.letsgetcheckedtask.supportClasses.interfaces.RapidAPI

object MainApp {

    private val SHARED_PREFS_TAG: String = "SharedPrefs"
    private val FAV_COUNTRY: String = "CountryID"

    private lateinit var rapidAPI: RapidAPI
    private lateinit var appApplication: Application
    private lateinit var appSharedPreferences: SharedPreferences
    private var countriesList: List<CountryModel> = listOf()


    fun initialize(application: Application) {
        appApplication = application
        appSharedPreferences =
            application.getSharedPreferences(SHARED_PREFS_TAG, Context.MODE_PRIVATE)
        rapidAPI = RetrofitInstance.getRapidAPI(getApiKey())
    }

    fun getApiKey() = this.appApplication.getString(R.string.api_key)


    fun getRapidAPI(): RapidAPI {
        return this.rapidAPI
    }

    /*============================ SHARED PREFES OPS ==================================*/
    fun setFavoriteCountry(name: String?) {
        name?.let {
            appSharedPreferences.edit().putString(FAV_COUNTRY, it).commit()
        }
    }

    fun hasFavorite(): Boolean {
        return appSharedPreferences.getString(FAV_COUNTRY, null) != null
    }

    fun getFavoriteCountry(): String? {
        return appSharedPreferences.getString(FAV_COUNTRY, null)
    }

    fun removeFavoriteCountry() {
        appSharedPreferences.edit().remove(FAV_COUNTRY)?.let { editor ->
            editor.commit()
        }
    }


    fun setCountriesList(list: List<CountryModel>) {
        this.countriesList = list
    }

    fun getCountriesList(): List<CountryModel> {
        return this.countriesList
    }
}
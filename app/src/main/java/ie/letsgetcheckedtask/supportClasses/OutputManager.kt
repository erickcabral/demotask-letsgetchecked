package ie.letsgetcheckedtask.supportClasses

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import ie.letsgetcheckedtask.R
import ie.letsgetcheckedtask.databinding.DialogCountryDetailsBinding
import ie.letsgetcheckedtask.databinding.DialogPositiveOnlyBinding
import ie.letsgetcheckedtask.models.binderModels.CardBinderModel
import ie.letsgetcheckedtask.models.binderModels.DialogCountryBinderModel
import ie.letsgetcheckedtask.models.binderModels.DialogPositiveModel

object OutputManager {


    private const val DIALOG_BACKGROUND_COLOR: Int = android.R.color.transparent
    private var dialogCountryDetails: AlertDialog? = null
    private var dialogPositiveOnly: AlertDialog? = null


    fun toastShort(message: String, parent_context: Context?) {
        Toast.makeText(parent_context, message, Toast.LENGTH_SHORT).show()
    }


    /*=========================== Dialog Country ==================================*/
    fun countryWindow(parent_context: Context?, cardBinderModel: CardBinderModel) {
        parent_context?.let {
            val dialogBinder = DataBindingUtil.inflate<DialogCountryDetailsBinding>(
                LayoutInflater.from(parent_context), R.layout.dialog_country_details, null, false
            )
            this.dialogCountryDetails =
                AlertDialog.Builder(parent_context).setView(dialogBinder.root)
                    .setCancelable(false)
                    .create().apply {
                        window!!.decorView.setBackgroundColor(
                            parent_context.resources.getColor(
                                DIALOG_BACKGROUND_COLOR, parent_context.theme
                            )
                        )
                    }


            val dismissButton: View.OnClickListener = View.OnClickListener {
                closeCountryDialog()
                cardBinderModel.cardClickListener.onDismiss()
            }

            dialogBinder.dialogBinder = DialogCountryBinderModel(cardBinderModel, dismissButton)

            dialogCountryDetails?.let {
                it.show()
            }
        }
    }

    fun closeCountryDialog() {
        this.dialogCountryDetails?.apply {
            if (this.isShowing) {
                this.dismiss()
            }
        }
        this.dialogCountryDetails = null
    }

    /*=========================== Dialog Positive Only ==================================*/
    fun dialogPositiveOnly(parent_context: Context?, dialogPositiveModel: DialogPositiveModel) {
        parent_context?.let {
            val binder = DataBindingUtil.inflate<DialogPositiveOnlyBinding>(
                LayoutInflater.from(parent_context),
                R.layout.dialog_positive_only,
                null,
                false
            )

            this.dialogPositiveOnly = AlertDialog.Builder(parent_context)
                .setView(binder.root)
                .create().apply {
                    window!!.decorView.setBackgroundColor(
                        parent_context.resources.getColor(
                            DIALOG_BACKGROUND_COLOR, parent_context.theme
                        )
                    )
                }

            if (dialogPositiveModel.clickListener == null) {
                dialogPositiveModel.clickListener = View.OnClickListener {
                    closePositiveOnlyDialog()
                }
            }

            binder.dialogModel = dialogPositiveModel

            this.dialogPositiveOnly?.let {
                it.show()
            }
        }
    }

    fun closePositiveOnlyDialog() {
        this.dialogPositiveOnly?.apply {
            if (this.isShowing) {
                this.dismiss()
            }
        }
        this.dialogPositiveOnly = null
    }
}
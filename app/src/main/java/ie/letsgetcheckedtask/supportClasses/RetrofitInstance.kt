package ie.letsgetcheckedtask.supportClasses

import com.google.gson.GsonBuilder
import ie.letsgetcheckedtask.supportClasses.interfaces.RapidAPI
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


private val BASE_URL = "https://restcountries-v1.p.rapidapi.com"

class RetrofitInstance {
    companion object Instance {
        fun getRapidAPI(apiKey: String) : RapidAPI {
            val interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }

            val customInterceptor = Interceptor { chain ->
                chain.withReadTimeout(5, TimeUnit.SECONDS)
                val urll = chain.request()
                    .url
                    .newBuilder().build()

                val request = chain.request()
                    .newBuilder()
                    .url(urll)
                    .get()
                    .addHeader("x-rapidapi-host", "restcountries-v1.p.rapidapi.com")
                    .addHeader("x-rapidapi-key", apiKey)
                    .build()
                return@Interceptor chain.proceed(request)
            }


            val client = OkHttpClient.Builder().apply {
                this.addInterceptor(customInterceptor)
                this.addInterceptor(interceptor)
            }.build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .build()
                .create(RapidAPI::
                class.java)
        }


    }

}
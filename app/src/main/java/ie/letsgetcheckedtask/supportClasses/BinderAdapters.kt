package ie.letsgetcheckedtask.supportClasses

import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ie.letsgetcheckedtask.adapters.CountriesAdapter
import ie.letsgetcheckedtask.models.adapterModel.AdapterCountriesModel
import java.text.DecimalFormat

@BindingAdapter("android:adapterCountries")
fun loadRecycler(recycler: RecyclerView, adapterCountriesModel: AdapterCountriesModel?) {
    if (adapterCountriesModel != null) {
        recycler.apply {
            layoutManager =
                LinearLayoutManager(recycler.context, LinearLayoutManager.VERTICAL, false)
            adapter = CountriesAdapter(adapterCountriesModel)
            adapter?.apply {
                notifyDataSetChanged()
            }
        }
    }
}

@BindingAdapter("android:formatPopulation")
fun formatPopulation(textView: TextView, number: String) {
    number.let {
        val format = DecimalFormat("###,###,###,###")
        val cc = number.toLong()
        val pop: String = format.format(cc).toString()
        textView.text = pop
    }
}
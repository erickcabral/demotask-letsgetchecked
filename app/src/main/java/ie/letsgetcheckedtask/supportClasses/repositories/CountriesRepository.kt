package ie.letsgetcheckedtask.supportClasses.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ie.letsgetcheckedtask.models.CountryModel
import ie.letsgetcheckedtask.supportClasses.interfaces.RapidAPI
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class CountriesRepository(val rapidAPI: RapidAPI, val defaultDispatcher: CoroutineDispatcher) {
    private val TAG = "<<_REPO_RAPID_>>"


    private val coroutineException = CoroutineExceptionHandler { coroutineContext, throwable ->
        val message = throwable.message
        lvdOnListError.postValue(message)
    }

    private val lvdCountriesListResponse: MutableLiveData<List<CountryModel>> =
        MutableLiveData<List<CountryModel>>()

    private val lvdOnListError: MutableLiveData<String> =
        MutableLiveData<String>()

//    private val lvdIsConnected: MutableLiveData<Boolean> = MutableLiveData<Boolean>()

    fun fetchCountries() {
        CoroutineScope(defaultDispatcher + coroutineException).launch {
            val list = rapidAPI.getAllCountries()
            list.let {
                withContext(Dispatchers.Default) {
                    lvdCountriesListResponse.postValue(list)
                }
            }
        }
    }
//
//
//    fun checkConnection(networkCapabilities: NetworkCapabilities?) {
//        CoroutineScope(defaultDispatcher).launch {
//            val isConnected = (networkCapabilities != null && networkCapabilities!!.hasCapability(
//                NetworkCapabilities.NET_CAPABILITY_INTERNET
//            ))
//            withContext(Dispatchers.Main) {
//                lvdIsConnected.postValue(isConnected)
//            }
//        }
//    }


    /*============================ GETTERS ============================*/
    fun getCountriesListResponseList(): LiveData<List<CountryModel>> {
        return this.lvdCountriesListResponse
    }

    fun getCountriesListResponse(): Flow<List<CountryModel>> {
        return flow {
            val list = rapidAPI.getAllCountries()
            emit(list)
        }
    }

    fun getListErrorStatus(): LiveData<String> {
        return this.lvdOnListError
    }

    fun clear() {
        defaultDispatcher.cancel()
    }
}
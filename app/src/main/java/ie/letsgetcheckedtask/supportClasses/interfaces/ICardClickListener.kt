package ie.letsgetcheckedtask.supportClasses.interfaces

import android.view.View

interface ICardClickListener {
    fun onCardClicked(view: View)
    fun onDismiss()
}

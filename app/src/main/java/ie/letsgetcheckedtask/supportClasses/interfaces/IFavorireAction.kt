package ie.letsgetcheckedtask.supportClasses.interfaces

import android.view.View

interface IFavoriteAction {
    fun onFavoriteClicked(view: View)
}
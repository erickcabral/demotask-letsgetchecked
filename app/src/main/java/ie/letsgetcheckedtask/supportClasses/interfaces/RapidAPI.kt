package ie.letsgetcheckedtask.supportClasses.interfaces

import ie.letsgetcheckedtask.models.CountryModel
import retrofit2.http.GET

interface RapidAPI {
//
//    @GET("/all")
//    suspend fun getAllCountries(): List<CountryModel>?
//
    @GET("/all")
    suspend fun getAllCountries(): List<CountryModel>
}
package ie.letsgetcheckedtask.supportClasses

import android.view.View
import androidx.navigation.Navigation

object NavigationManager {

    fun setNavigation(view: View, navigatorID: Int){
        Navigation.findNavController(view).setGraph(navigatorID)
    }

}
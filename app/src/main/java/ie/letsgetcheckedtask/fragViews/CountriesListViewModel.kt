package ie.letsgetcheckedtask.fragViews

import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ie.letsgetcheckedtask.models.CountryModel
import ie.letsgetcheckedtask.models.binderModels.CardBinderModel
import ie.letsgetcheckedtask.supportClasses.MainApp

class CountriesListViewModel : ViewModel() {
    private val TAG = "<<_COUNTRIES_LIST_VM_>>"
    var itemPosition: Int = -1
    var selectedItem: CardBinderModel? = null
    private var viewSelected: View? = null

    val lvdFavoriteModel: MutableLiveData<CountryModel> = MutableLiveData()
    val lvdRemovedModel: MutableLiveData<CountryModel> = MutableLiveData()

    fun updateFavorite() {
        selectedItem?.let { item ->
            val model = MainApp.getCountriesList().get(item.listPosition)
            if (MainApp.getFavoriteCountry() == model.name) {
                MainApp.removeFavoriteCountry()
                model.isFavorite = false
            } else {
                MainApp.setFavoriteCountry(model.name)
                model.isFavorite = true
            }
            this.itemPosition = item.listPosition
            this.lvdFavoriteModel.postValue(model)
        }
    }

    fun clearCurrentFavorite() {
        itemPosition = 0
        for (model in MainApp.getCountriesList()) {
            if (model.name == MainApp.getFavoriteCountry()) {
                model.isFavorite = false
                MainApp.removeFavoriteCountry()
                lvdRemovedModel.postValue(model)
                return
            }
            itemPosition++
        }
    }

    fun setViewClicked(view: View) {
        this.viewSelected = view
        this.setItemLocked()
    }

    fun setItemLocked() {
        this.viewSelected?.let { card ->
            card.isClickable = !card.isClickable
            Log.i(TAG, "View is locked -> ${card.isClickable}")
        }
    }


    /*==================== GETTERS ======================*/
    fun getFavoritedModel(): LiveData<CountryModel> {
        return this.lvdFavoriteModel
    }

    fun getRemovedModel(): LiveData<CountryModel> {
        return this.lvdRemovedModel
    }


}
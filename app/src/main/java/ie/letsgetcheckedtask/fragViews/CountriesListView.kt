package ie.letsgetcheckedtask.fragViews

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import ie.letsgetcheckedtask.R
import ie.letsgetcheckedtask.databinding.FragViewCountriesListBinding
import ie.letsgetcheckedtask.models.CountryModel
import ie.letsgetcheckedtask.models.adapterModel.AdapterCountriesModel
import ie.letsgetcheckedtask.models.binderModels.CardBinderModel
import ie.letsgetcheckedtask.models.binderModels.DialogPositiveModel
import ie.letsgetcheckedtask.supportClasses.MainApp
import ie.letsgetcheckedtask.supportClasses.OutputManager
import ie.letsgetcheckedtask.supportClasses.interfaces.ICardClickListener
import ie.letsgetcheckedtask.supportClasses.interfaces.IFavoriteAction

class CountriesListView : Fragment(), IFavoriteAction, ICardClickListener {

    private lateinit var viewBinder: FragViewCountriesListBinding
    private val viewModel by activityViewModels<CountriesListViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        this.viewBinder = DataBindingUtil.inflate<FragViewCountriesListBinding>(
            inflater,
            R.layout.frag_view_countries_list,
            container,
            false
        )

        this.onFavoriteUpdated()
        this.onFavoriteRemoved()

        return this.viewBinder.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        MainApp.getCountriesList().let {
            setViewBinder(it)
        }

    }

    private fun onFavoriteUpdated() {
        this.viewModel.getFavoritedModel().observe(requireActivity(), Observer { model ->
            model?.let {
                displayUpdateDialog(it)
                updateRecyclerItem(model)
            }
        })
    }

    private fun onFavoriteRemoved() {
        this.viewModel.getRemovedModel().observe(requireActivity(), Observer { model ->
            model?.let {
                updateRecyclerItem(model)
                this.viewModel.updateFavorite()
            }
        })
    }


    fun displayUpdateDialog(countryModel: CountryModel) {
        OutputManager.closeCountryDialog()

        var title: String = countryModel.name.toString()
        var message: String

        if (countryModel.isFavorite) {
            message = "Country added successfully"
        } else {
            message = "Country removed successfully"
        }
        val dialogPositiveModel = DialogPositiveModel(title, message)
        OutputManager.dialogPositiveOnly(context, dialogPositiveModel)
    }

    fun setViewBinder(countriesList: List<CountryModel>) {
        val adapterModel = AdapterCountriesModel(countriesList, this)
        this.viewBinder.binderModel = adapterModel
    }

    fun updateRecyclerItem(model: CountryModel) {
        this.viewBinder.recyclerCountries.adapter?.apply {
            val itemPosition = viewModel.itemPosition
            notifyItemChanged(itemPosition, model)
        }
    }


    /*==================== LISTENERS ====================*/
    override fun onFavoriteClicked(view: View) {
        view.tag?.let {
            var cardBinderModel: CardBinderModel = view.tag as CardBinderModel
            this.viewModel.selectedItem = cardBinderModel
            if (MainApp.hasFavorite() && MainApp.getFavoriteCountry() != cardBinderModel.countryModel.name) {
                this.viewModel.clearCurrentFavorite()
            } else {
                this.viewModel.updateFavorite()
            }
        }
    }

    override fun onCardClicked(view: View) {
        this.viewModel.setViewClicked(view)
        val cardBinderModel = view.tag as CardBinderModel
//        OutputManager.toastShort("Card Country Clicked > ${cardBinderModel.name}", context)
        OutputManager.countryWindow(context, cardBinderModel)
    }

    override fun onDismiss() {
        this.viewModel.setItemLocked()
    }
}
package ie.letsgetcheckedtask.testUtils

import ie.letsgetcheckedtask.models.CountryModel
import ie.letsgetcheckedtask.supportClasses.interfaces.RapidAPI
import org.mockito.Mockito

const val WARNING_MESSAGE = "ERROR WHILE RETRIEVING COUNTRIES"

object TestUtils {

    inline fun <reified T> mock(): T = Mockito.mock(T::class.java)

    fun getCountries(): List<CountryModel> {
        return listOf(
            CountryModel("name 1", "native1", "capital1"),
            CountryModel("name 2", "native2", "capital2"),
            CountryModel("name 3", "native3", "capital3")
        )
    }

    class RapidApi_TD : RapidAPI {
        companion object {
            var fail = false
        }

        override suspend fun getAllCountries(): List<CountryModel> {
            if (fail) {
                val exception = object :Exception(){
                    override val message: String?
                        get() = WARNING_MESSAGE
                }
                throw exception
            } else {
                return getCountries()
            }
        }
    }
}

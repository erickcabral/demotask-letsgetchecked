package ie.letsgetcheckedtask.supportClasses.repositories

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.asLiveData
import ie.letsgetcheckedtask.models.CountryModel
import ie.letsgetcheckedtask.testUtils.TestUtils
import ie.letsgetcheckedtask.testUtils.TestUtils.mock
import ie.letsgetcheckedtask.testUtils.WARNING_MESSAGE
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CountriesRepositoryTest {

    @get:Rule
    val ins = InstantTaskExecutorRule()
    private val testDispatcher = TestCoroutineDispatcher()

    //SYSTEM UNDER TEST
    lateinit var SUT: CountriesRepository

    //TEST DOUBLE
    private val rapidAPITD = TestUtils.RapidApi_TD()

    //MOCKED
    lateinit var mockedCountryListObserver: Observer<List<CountryModel>?>
    lateinit var mockedStringObserver: Observer<String>

    //LIVE DATA
    lateinit var lvdCountriesList: LiveData<List<CountryModel>>
    lateinit var lvdErrorCatcher: LiveData<String>

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        this.SUT = CountriesRepository(rapidAPITD, testDispatcher)

        this.mockedCountryListObserver = mock()
        this.mockedStringObserver = mock()

        lvdCountriesList = mock()
        lvdErrorCatcher = mock()
    }

    @After
    fun tearDown() {
        testDispatcher.cleanupTestCoroutines()
        SUT.clear()
        Dispatchers.resetMain()
        this.lvdCountriesList.observeForever(mockedCountryListObserver)
        this.lvdErrorCatcher.observeForever(mockedStringObserver)
    }

    @Test
    fun liveDataCountriesList_success() {
        success()
        runBlocking(testDispatcher) {
            SUT.fetchCountries()
        }
        this.lvdCountriesList = SUT.getCountriesListResponseList()
        this.lvdCountriesList.observeForever(mockedCountryListObserver)

        val captor = ArgumentCaptor.forClass(List::class.java)
        captor.run {
            verify(mockedCountryListObserver, times(1)).onChanged(capture() as List<CountryModel>?)
            val retrievedList = value as List<CountryModel>
            assertEquals(TestUtils.getCountries(), retrievedList)
            assertEquals(TestUtils.getCountries().get(0).name, retrievedList.get(0).name)
        }
    }

    @Test
    fun liveDataCountriesList_error() {
        failure()
        runBlocking(testDispatcher) {
            SUT.fetchCountries()
        }
        SUT.getListErrorStatus().observeForever(mockedStringObserver)
        val captor2 = ArgumentCaptor.forClass(String::class.java)
        captor2.run {
            verify(mockedStringObserver, times(1)).onChanged(capture())
            val message = value
            assertTrue(message.equals(WARNING_MESSAGE))
        }
    }

    @Test
    fun flow_countriesList_success() {
        runBlocking(testDispatcher) {
            SUT.getCountriesListResponse()
        }

        this.lvdCountriesList = SUT.getCountriesListResponse().asLiveData()
        this.lvdCountriesList.observeForever(mockedCountryListObserver)

        val captor = ArgumentCaptor.forClass(List::class.java)
        captor.run {
            verify(mockedCountryListObserver, times(1)).onChanged(capture() as List<CountryModel>?)
            val retrievedList = value as List<CountryModel>
            assertEquals(TestUtils.getCountries(), retrievedList)
            assertEquals(TestUtils.getCountries().get(0).name, retrievedList.get(0).name)
        }
    }

    // HELPERS //
    fun failure() {
        TestUtils.RapidApi_TD.fail = true
    }

    fun success() {
        TestUtils.RapidApi_TD.fail = false
    }
}